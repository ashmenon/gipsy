<?php
	$page_title = 'Project Gipsy - Gipsy Tracking System';
?>
<?php include_once('includes/header.php'); ?>
	<div class="heightblock1"></div>
	<div id="map_section" class="large_section border1 bgcolor1">
		<h3 class="subsection_title">Gipsy Tracking System</h3>
		<div class="subsection_subtitle">Geolocated reporting capability to view captured radio parameters and service test results</div>
		<div class="clear heightblock1"></div>
		<a class="accordion_toggle toggle_active" rel="chart">Statistics (Last 12 Months)</a>
		<div class="accordion_container container_active" rel="chart">
			<div id="chart_holder">
				<ul id="chart_scale">					
				</ul>
				<div id="chart">
					<div class="chart_block chart_block_first">
						<div class="chart_bar chart_bar_gsm" style="height: 20%;"></div>
						<div class="chart_bar chart_bar_wcdma" style="height: 60%;"></div>
						<div class="chart_bar chart_bar_events"  style="height: 70%;"></div>
					</div>
					<div class="chart_block">
						<div class="chart_block_loading"></div>
					</div>
				</div>
				<div id="chart_legend" class="border1 bgcolor2">
					<h5>Legend</h5>
					<div class="chart_legend_main_container">
						
						<div class="chart_legend_container">
							<div class="colorblock colorblock_gsm"></div>
							<div class="chart_legend_key">GSM Data</div>
						</div>
						
						<div class="chart_legend_container">
							<div class="colorblock colorblock_wcdma"></div>
							<div class="chart_legend_key">WCDMA Data</div>
						</div>
						
						<div class="chart_legend_container">
							<div class="colorblock colorblock_events"></div>
							<div class="chart_legend_key">Event Data</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<a class="accordion_toggle toggle_inactive" rel="map">Map</a>
		<div class="accordion_container container_inactive" rel="map">
			<div id="map_holder" class="border2"></div>
			<div id="map_filter_holder">
				<form id="map_filter_form" class="map_form">			
					<div>
						<h4 class="nomargin">Gipsy Search</h4>
						<div class="field_header">Date Range</div>
						<div class="clear"></div>
						
						<div class="field_holder field_holder_first">
							<label for="filter_date_range_start">From</label>
							<input class="datepicker_field" type="text" name="filter_date_range_start" />
						</div>
						
						<div class="field_holder field_holder_last">
							<label for="filter_date_range_end">To</label>
							<input class="datepicker_field" type="text" name="filter_date_range_end" />
						</div>
						
						<div id="below_date_fields" class="clear"></div>
											
						<div class="field_header">Time Range</div>
						<div class="clear"></div>
						
						<div class="field_holder field_holder_first">
							<label for="filter_time_range_start">From</label>
							<input class="timepicker_field" type="text" name="filter_time_range_start" />
						</div>
						
						<div class="field_holder field_holder_last">
							<label for="filter_time_range_end">To</label>
							<input class="timepicker_field" type="text" name="filter_time_range_end" />
						</div>
						<div id="below_time_fields" class="clear"></div>
						
						
						<div class="field_header">Gipsy ID</div>
						<select name="filter_probe_id" disabled="disabled">
							<option value="">Please Wait...</option>
						</select>
						<div id="below_probe_select" class="clear"></div>
						
						<div class="field_header">Device ID</div>
						<select name="filter_device_id" disabled="disabled">
							<option value="">Please Select a Gipsy ID</option>
						</select>
						<div id="below_device_select" class="clear"></div>
						
						<div class="field_header">Event Type</div>
						<select name="filter_event_type" disabled="disabled">
							<option value="" selected="selected">Please Select a Device ID</option>						
						</select>
						<div class="clear"></div>
						
						<div class="field_header">Technology</div>
						<div class="radio_buttons_holder">
							<label>
								<input checked="checked" type="radio" name="filter_technology" value="all" />
								<span>All</span>
							</label>
							<label>
								<input type="radio" name="filter_technology" value="gsm" />
								<span>GSM</span>
							</label>
							<label>
								<input type="radio" name="filter_technology" value="wcdma" />
								<span>WCDMA</span>
							</label>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
						<div class="form_buttons">
							<a href="#" id="filter_form_submit_button" class="form_button">View</a>
							<a href="#" id="filter_form_reset_button" class="form_button">Reset</a>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
						<script type="text/javascript" language="javascript">initFilterForm();</script>
					</div>
				</form>
			</div>
			<div id="toggle_buttons_holder">
				<div class="toggle_button button_gsm no_button"></div>
				<div class="toggle_button button_wcdma no_button"></div>
				<div class="toggle_button button_gps no_button"></div>
				<div class="toggle_button button_events no_button"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	
	<div class="heightblock1"></div>
	
<?php include_once('includes/footer.php'); ?>
		
			
			
		