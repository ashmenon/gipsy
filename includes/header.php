<!DOCTYPE html>
<html>
  <head>
	<title><?php echo isset($page_title) ? $page_title : ''; ?></title>
    <script type="text/javascript" language="javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" language="javascript" src="/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" language="javascript" src="/js/jquery-ui-1.8.20.custom.min.js"></script>
    <script type="text/javascript" language="javascript" src="/js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" language="javascript" src="/js/jquery.qtip-1.0.0-rc3.min.js"></script>	
    <script type="text/javascript" language="javascript" src="/js/gipsy.js"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="/css/fonts.css"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="/css/grid.css"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="/css/smoothness/jquery-ui-1.8.20.custom.css"></script>
	<link rel="stylesheet" type="text/css" media="screen" href="/css/datepicker.css"></script>
	 <link href="/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" media="screen" href="/css/gipsy.css"></script>
	
  </head>
  <body>
	<div id="page_background">
		<div id="page_holder">
			<div id="page">
				<div id="navigation">
					<div id="logo" class="bgcolor1 border1">
						<a href="/" title="Return to Main Page">
							<img src="/images/logo_large.png" alt="Gipsy" />
						</a>
					</div>
					<a class="navigation_button bgcolor1 border1" href="/">
						<span>
							<span>Gipsy</span>
							<span>Tracking</span>
							<span>System</span>
						</span>
					</a>
					<a class="navigation_button bgcolor1 border1" href="/page2.php">
						<span>
							<span>Fleet</span>
							<span>Monitoring</span>
							<span>System</span>
						</span>
					</a>
					
					<div id="navigation_text">
						Geolocated Information Provisioning System (GIPSY)<br />
						Quality of Service information on your Wireless Operators
					</div>
					<div class="clear"></div>
				</div>
