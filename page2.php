<?php
	$page_title = 'Project Gipsy - Fleet Monitor System';
?>
<?php include_once('includes/header.php'); ?>
	<script type="text/javascript" language="javascript">
		jQuery(document).ready(function(){
			initMap();
			getUserLocation();
		});
	</script>
	<div class="heightblock1"></div>
	<div id="map_section" class="large_section border1 bgcolor1">
		<h3 class="subsection_title">Fleet Monitoring System</h3>
		<div class="subsection_subtitle">Administrative Interface for Fleet Managers</div>
		<div class="clear heightblock1"></div>
		<div id="map_holder" class="border2"></div>
		<div id="map_filter_holder">
			<form id="fleet_tracker_form" class="map_form">			
				<div>
					<h4 class="nomargin">Gipsy Search</h4>
					<div class="field_header">Gipsy ID</div>
					<select name="filter_probe_id" disabled="disabled" multiple="multiple">
						<option value="">Please Wait...</option>
					</select>
					<div class="clear"></div>
					<div class="form_buttons">
						<a href="#" id="fleet_form_submit_button" class="form_button">View</a>
						<div class="clear"></div>
					</div>	
					<script type="text/javascript" language="javascript">initFleetForm();</script>
					
				</div>
			</form>
		</div>
		<div class="clear"></div>
	</div>
	
	<div class="heightblock1"></div>
	
<?php include_once('includes/footer.php'); ?>
		
			
			
		