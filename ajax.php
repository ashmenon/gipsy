<?php

$f = isset($_GET['fname']) ? $_GET['fname'] : '';
$config = array(
	'domain' => 'http://202.71.97.98'
);

function get_data($key, $default = ''){
	if(isset($_GET[$key])){
		return $_GET[$key]; 
	} else if(isset($_POST[$key])){
		return $_POST[$key];
	} else {
		return $default;
	};	
};

switch($f){
	case 'getprobes':
		get_probes();
		break;
	case 'getdevices':
		get_devices(get_data('probe_id'));		
		break;
	case 'getmapdata':
		get_map_data(get_data('start_date'), get_data('end_date'), get_data('probe_id'), get_data('start_time'), get_data('end_time'), get_data('device_id'), get_data('technology'));
		break;
	case 'getprobefleetdata':
		get_probe_fleet_data(get_data('probe_id'),get_data('start_date'),get_data('end_date'), get_data('start_time'), get_data('end_time'));
		break;
	case 'gettasks':
		get_tasks(get_data('device_id'));
		break;
	case 'getevents':
		get_events(get_data('device_id'),get_data('gps_id'),get_data('start_date'),get_data('end_date'), get_data('start_time'), get_data('end_time'));
		break;
	case 'getchart':
		get_chart(get_data('y'),get_data('m'));
		break;
	case 'getgsmdata':
		get_gsm_data(get_data('gps_id'),get_data('device_id'));
		break;
	case 'getgneighbourdata':
		get_g_neighbour_data(get_data('gps_id'),get_data('device_id'));
		break;
	case 'getwcdmadata':
		get_wcdma_data(get_data('gps_id'),get_data('device_id'));
		break;
	case 'getwmonitoredinterfreqdata':
		get_w_monitored_inter_freq_data(get_data('gps_id'),get_data('device_id'));
		break;
	case 'getwmonitoredintrafreqdata':
		get_w_monitored_intra_freq_data(get_data('gps_id'),get_data('device_id'));
		break;
	case 'getwmonitoredinterratdata':
		get_w_monitored_inter_rat_data(get_data('gps_id'),get_data('device_id'));
		break;
	case 'getwdetecteddata':
		get_w_detected_data(get_data('gps_id'),get_data('device_id'));
		break;
	case 'newtask':
		post_new_task(get_data('device_id'),get_data('type'),get_data('option'),get_data('parameter'));
		break;
	default:
		die('Invalid function definition');
		break;	
};


function get_probes(){
	global $config;
	$url = $config['domain'] . '/probes.json';
	$data = file_get_contents($url);
	die($data);	
};

function get_devices($probe_id){
	global $config;
	$url = $config['domain'] . '/devices.json?probeid=' . $probe_id;
	$data = file_get_contents($url);
	die($data);
};


function get_map_data($start_date, $end_date, $probe_id, $start_time = '000000', $end_time = '000000', $device_id = 'all', $technology = 'all'){
	//$start_date = '20090213';
	//$start_time = '233130';
	global $config;
	$data = array();
	if($device_id != 'all' && (int)$device_id > 0){		
		if($technology == 'gsm' || $technology == 'all'){
			$url = $config['domain'] . '/gsms.json?fromdatetime=' . $start_date . $start_time . '&todatetime=' . $end_date . $end_time . '&deviceid=' . $device_id;
			$data['gsm'] = json_decode(file_get_contents($url),true);
		};
		
		if($technology == 'wcdma' || $technology == 'all'){
			$url = $config['domain'] . '/wcdmas.json?fromdatetime=' . $start_date . $start_time . '&todatetime=' . $end_date . $end_time . '&deviceid=' . $device_id;
			$data['wcdma'] = json_decode(file_get_contents($url),true);
		};
	} else {
		$url = $config['domain'] . '/gps.json?fromdatetime=' . $start_date . $start_time . '&todatetime=' . $end_date . $end_time . '&probeid=' . $probe_id;
		$data['gps'] = json_decode(file_get_contents($url),true);
	};
	
	die(json_encode($data));
};

function get_probe_fleet_data($probe_id, $start_date = '', $end_date = '', $start_time = '', $end_time = ''){
	global $config;
	$url = $config['domain'] . '/gps.json?probeid=' . $probe_id;
	if(strlen($start_date) && strlen($end_date) && strlen($start_time) && strlen($end_time)){
		$url .= '&fromdatetime=' . $start_date . $start_time . '&todatetime=' . $end_date . $end_time;
	};
	$data = file_get_contents($url);
	die($data);
};

function get_tasks($device_id){	
	global $config;
	$url = $config['domain'] . '/tasks.json?deviceid=' . $device_id;
	$data = file_get_contents($url);
	die($data);
};

function get_events($device_id, $gps_id = '', $start_date = '', $end_date = '', $start_time = '', $end_time = ''){
	global $config;
	//$start_date = '20090213';
	//$start_time = '233130';
	$url = $config['domain'] . '/events.json?deviceid=' . $device_id . (($gps_id) ? '&gpsid=' . $gps_id : '') . (($start_date && $start_time) ? '&fromdatetime=' . $start_date . $start_time : '') . (($end_date && $end_time) ? '&todatetime=' . $end_date . $end_time : '');
	$data = file_get_contents($url);
	die($data);
};

function get_chart($year,$month){
	global $config;
	if(!strlen($year) && !strlen($month)) return false;
	if(strlen($month) == 1){ $month = '0' . $month; };
		
	$params = array();
	if(strlen($year)) $params[] = 'year=' . ((strlen($year)==4) ? substr($year,2) : $year);
	if(strlen($month)) $params[] = 'month=' . $month;
		
	$url = $config['domain'] . '/counts.json?' . join('&',$params);
	$data = file_get_contents($url);
	die($data);
};




function get_gsm_data($gps_id,$device_id){
	global $config;
	$url = $config['domain'] . '/gsms.json?gpsid=' . $gps_id . '&deviceid=' . $device_id;
	$data = file_get_contents($url);
	die($data);
};

function get_g_neighbour_data($gps_id,$device_id){
	global $config;
	$url = $config['domain'] . '/g_neighbours.json?gpsid=' . $gps_id . '&deviceid=' . $device_id;
	$data = file_get_contents($url);
	die($data);
};

function get_wcdma_data($gps_id,$device_id){
	global $config;
	$url = $config['domain'] . '/wcdmas.json?gpsid=' . $gps_id . '&deviceid=' . $device_id;
	$data = file_get_contents($url);
	die($data);
};

function get_w_monitored_inter_freq_data($gps_id,$device_id){
	global $config;
	$url = $config['domain'] . '/w_monitored_inter_freqs.json?gpsid=' . $gps_id . '&deviceid=' . $device_id;
	$data = file_get_contents($url);
	die($data);
};

function get_w_monitored_intra_freq_data($gps_id,$device_id){
	global $config;
	$url = $config['domain'] . '/w_monitored_intra_freqs.json?gpsid=' . $gps_id . '&deviceid=' . $device_id;
	$data = file_get_contents($url);
	die($data);
};

function get_w_monitored_inter_rat_data($gps_id,$device_id){
	global $config;
	$url = $config['domain'] . '/w_monitored_inter_rats.json?gpsid=' . $gps_id . '&deviceid=' . $device_id;
	$data = file_get_contents($url);
	die($data);
};

function get_w_detected_data($gps_id,$device_id){
	global $config;
	$url = $config['domain'] . '/w_detecteds.json?gpsid=' . $gps_id . '&deviceid=' . $device_id;
	$data = file_get_contents($url);
	die($data);
};

function post_new_task($device_id, $type, $option, $parameter){
	$task = array(
		'task[device_id]' => $device_id,
		'task[Type]' => $type,
		'task[Option]' => $option,
		'task[Parameter]' => $parameter
	);
	
	// create a new cURL resource
	$ch = curl_init();

	// set URL and other appropriate options
	//curl_setopt($ch, CURLOPT_URL, $config['domain'] . '/tasks');
	curl_setopt($ch, CURLOPT_URL, 'http://202.71.97.98/tasks');
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $task);

	// grab URL and pass it to the browser
	curl_exec($ch);

	// close cURL resource, and free up system resources
	curl_close($ch);
	
	
	
	
	
	
};





?>