jQuery.noConflict();

var map, mapBounds;
var gsmMarkers = [], wcdmaMarkers = [], gpsMarkers = [], eventMarkers = [], fleetMarkers = [];
var mapLoaded = false;
var fleetData = {}, fleetInterval, activeFleetMarkerId = 0;

jQuery(document).ready(function(){
	//initMap();
	//getUserLocation();	
	initAccordion();
	renderChart();
	//initResizeHeight();
	
	
	jQuery('.accordion_container[rel="map"]').bind('container_open',function(){
		if(!mapLoaded){
			initMap();
			getUserLocation();
			mapLoaded = true;
		};
	});
});

function getUserLocation(){
	jQuery(document).ready(function(){			
		if (navigator.geolocation) {
			//using javascript to get browser location
			navigator.geolocation.getCurrentPosition(function(position){
				var lat = position.coords.latitude;
				var lng = position.coords.longitude;
				var latLng = new google.maps.LatLng(lat,lng);
				map.setCenter(latLng);
				map.setZoom(16);
			},function(error){
				//use error.code to determine what went wrong
				console.log(error.code);
			},{enableHighAccuracy:true, maximumAge:30000, timeout:27000});
		};		
	});
};

function initMap() {
	var myOptions = {
	  center: new google.maps.LatLng(0, 0),
	  zoom: 8,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_holder"),myOptions);
}

function initResizeHeight(){
	var windowHeight = jQuery(window).height();
	var pageHeight = jQuery('#page').height();	
	if(pageHeight < windowHeight){
		//Page is shorter that window. 
		jQuery('#page').css({
			'min-height': windowHeight			
		}).addClass('minheightfix');
		
		var footer = jQuery('#footer');
		var footerWidth = footer.width();
		footer.css('width',footerWidth).addClass('dock-footer');		
	};
};

function initFilterForm(){
	var form = jQuery('#map_filter_form');
	if(!form.length) return false;
	form.find('.datepicker_field').datepicker({
		dateFormat: 'dd/mm/yy'		
	});
	form.find('.timepicker_field').timepicker({
		showSecond: true,
		timeFormat: 'hh:mm:ss'
	});
	
	form.submit(function(){
		return validateFilterForm();
	});
	
	form.find('#filter_form_submit_button').click(function(){
		form.submit();
		return false;
	});
	
	form.find('#filter_form_reset_button').click(function(){
		form[0].reset();
		form.find('.error_message_box').remove();
		return false;
	});
	
	form.find('select[name="filter_device_id"]').change(function(){
		var value = jQuery(this).val();
		var eventTypeField = jQuery('select[name="filter_event_type"]');				
		if(value != 'all' && value != ''){
			eventTypeField.find('option:first-child').html('Please select an Event type').attr('value','');
			eventTypeField.find('option:gt(0)').remove();
			eventTypeField.append('<option value="VOICE">Voice</option>');
			//eventTypeField.append('<option value="DATA">Data</option>');
			eventTypeField.append('<option value="HTTP">HTTP</option>');
			eventTypeField.append('<option value="FTP">FTP</option>');
			eventTypeField.append('<option value="SMS">SMS</option>');
			eventTypeField.removeAttr('disabled');
		} else {
			eventTypeField.find('option:first-child').html('Please select a Device ID');
			eventTypeField.find('option:gt(0)').remove();
			eventTypeField.attr('disabled','disabled');
		};
	});
	
	getProbeIds(true);
}

function getProbeIds(allProbes){
	var dropDown = jQuery('select[name="filter_probe_id"]');
	var probeLoader = addLoaderMessage('Getting probes...');
	jQuery.getJSON('/ajax.php?fname=getprobes',function(data){
		removeLoaderMessage(probeLoader);
		if(data.length >= 1){			
			jQuery.each(data,function(index,item){
				var mac = item.probe.ProbeMAC;
				if(dropDown.length){
					var newOption = jQuery('<option />');
					newOption.attr('value',item.probe.id).html('Gipsy ' + item.probe.id).appendTo(dropDown);
				};			
			});
			if(dropDown.length){
				dropDown.removeAttr('disabled');
				if(allProbes){
					dropDown.find('option:first-child').html('Please select a Gipsy ID').attr('value','');
				} else {
					dropDown.find('option:first-child').remove();
				};
				dropDown.change(function(){
					getDeviceIds(jQuery(this).val());					
				});
			};
		} else {
			if(dropDown.length){
				dropDown.find('option').html('No probes available');
			};
		};
	});
};

function getDeviceIds(probeId, allDevices){
	var dropDown = jQuery('select[name="filter_device_id"]');
	if(probeId == 'all' || probeId == ''){
		dropDown.find('option:gt(0)').remove();
		dropDown.attr('disabled','disabled');
		dropDown.find('option:first-child').html('Please select a Gipsy ID');
		return true;
	};	
	dropDown.find('option:first-child').html('Please Wait...');
	var devicesLoader = addLoaderMessage('Getting devices...');
	jQuery.getJSON('/ajax.php?fname=getdevices&probe_id=' + probeId,function(data){
		removeLoaderMessage(devicesLoader);
		jQuery.each(data,function(index,item){
			var mac = item.device.DeviceMAC;
			var newOption = jQuery('<option />');
			newOption.attr('value',item.device.id).html('Device ' + item.device.id).appendTo(dropDown);
		});
		dropDown.removeAttr('disabled');
		dropDown.find('option:first-child').html('All Devices').attr('value','all');
	});
};

function validateFilterForm(){	
	var form = jQuery('#map_filter_form');
	if(!form.length) return false;
	form.find('.error_message_box').remove();
	var success = true;	
	
	var dateFields = form.find('.datepicker_field');
	var dateFieldsHasValue = true;
	dateFields.each(function(index,item){
		if(!jQuery(item).val().length) dateFieldsHasValue = false;		
	});
	
	if(!dateFieldsHasValue){
		jQuery('<div />').addClass('error_message_box border3 bgcolor2 fontcolor1').hide().html('Please select starting and ending dates').insertAfter(form.find('#below_date_fields'));
		success = false;
	} else {
		//Get value
		var fromDate = jQuery(dateFields[0]).val();
		var toDate = jQuery(dateFields[1]).val();
		//Split into bits
		var fromDateParts = fromDate.split('/');
		var toDateParts = toDate.split('/');
		//Rejoin into yyyymmdd format
		var fromDateStr = fromDateParts[2] + fromDateParts[1] + fromDateParts[0];
		var toDateStr = toDateParts[2] + toDateParts[1] + toDateParts[0];
		//Convert to integers.
		fromDateInt = parseInt(fromDateStr,10);
		toDateInt = parseInt(toDateStr,10);
		
		//Compare
		if(fromDateInt >= toDateInt){
			//Show error.
			jQuery('<div />').addClass('error_message_box border3 bgcolor2 fontcolor1').hide().html('Start date must be earlier than end date').insertAfter(form.find('#below_date_fields'));
			success = false;
		};
		
		var timeFields = form.find('.timepicker_field');
		var timeFieldsHasValue = true;
		timeFields.each(function(index,item){
			if(!jQuery(item).val().length) timeFieldsHasValue = false;
		});
		
		if(timeFieldsHasValue){
			var fromTimeStr = jQuery(timeFields[0]).val().replace(/:/g,'');
			var toTimeStr = jQuery(timeFields[1]).val().replace(/:/g,'');
			var fromDateTimeStr = fromDateStr + fromTimeStr;
			var toDateTimeStr = toDateStr + toTimeStr;
			var fromDateTimeInt = parseInt(fromDateTimeStr,10);
			var toDateTimeInt = parseInt(toDateTimeStr,10);
			
			if(fromDateTimeInt >= toDateTimeInt){
				jQuery('<div />').addClass('error_message_box border3 bgcolor2 fontcolor1').hide().html('Start time must be earlier than end time').insertAfter(form.find('#below_time_fields'));
				success = false;
			};			
		} else {
			var fromTimeStr = '000000';
			var toTimeStr = '000000';
		};		
	};
	
	var probeId = jQuery('select[name="filter_probe_id"]').val();
	if(!probeId.length){
		jQuery('<div />').addClass('error_message_box border3 bgcolor2 fontcolor1').hide().html('Please select a Gipsy ID').insertAfter(form.find('#below_probe_select'));
		success = false;
	};
	
	jQuery('.error_message_box').slideDown();
	
	//All seems to be okay. Let's send the query.
	
	if(!success) return false;
	
	var probeId = jQuery('select[name="filter_probe_id"]').val();
	var deviceId = jQuery('select[name="filter_device_id"]').val();
	var eventType = jQuery('select[name="filter_event_type"]').val();
	var tech = jQuery('input[name="filter_technology"]:checked').val();
	
	
	
	if(deviceId != 'all'){
		switch(tech){
			case 'all':
				var gsmLoader = addLoaderMessage('Getting GSM data...');
				var wcdmaLoader = addLoaderMessage('Getting WCDMA data...');
				showToggle(['gsm','wcdma']);
				break;
			case 'gsm':
				var gsmLoader = addLoaderMessage('Getting GSM data...');
				showToggle(['gsm']);
				break;
			case 'wcdma':
				var wcdmaLoader = addLoaderMessage('Getting WCDMA data...');
				showToggle(['wcdma']);
				break;
			default:
			
				break;
		};
	} else {
		var gpsLoader = addLoaderMessage('Getting GPS data...');
		showToggle(['gps']);
	};
	
	jQuery.each(gsmMarkers,function(index,item){
			item.setMap(null);
	});
	gsmMarkers = [];
	
	jQuery.each(wcdmaMarkers,function(index,item){
		item.setMap(null);
	});
	wcdmaMarkers = [];
	
	jQuery.each(eventMarkers,function(index,item){
		item.setMap(null);
	});
	eventMarkers = [];
	
	jQuery.each(gpsMarkers,function(index,item){
		item.setMap(null);
	});
	gpsMarkers = [];
	
	mapBounds = null;
	
	jQuery.getJSON('/ajax.php?fname=getmapdata',{
		start_date 		: fromDateStr,
		end_date 		: toDateStr,
		start_time 		: fromTimeStr,
		end_time 		: toTimeStr,
		probe_id 		: probeId,
		device_id 		: deviceId,
		technology		: tech
	},function(data){
		if(deviceId != 'all'){
			switch(tech){
				case 'all':
					removeLoaderMessage(gsmLoader);
					removeLoaderMessage(wcdmaLoader);
					displayGSMDataOnMap(data.gsm);
					displayWCDMADataOnMap(data.wcdma);
					showToggle(['gsm','wcdma']);
					break;
				case 'gsm':
					removeLoaderMessage(gsmLoader);					
					displayGSMDataOnMap(data.gsm);
					showToggle(['gsm']);
					break;
				case 'wcdma':
					removeLoaderMessage(wcdmaLoader);
					displayWCDMADataOnMap(data.wcdma);
					showToggle(['wcdma']);
					break;
				default:
				
					break;
			};
					
		} else {
			removeLoaderMessage(gpsLoader);					
			displayGPSDataOnMap(data.gps);	
			showToggle(['gps']);
		};
	});
	
	
	
	if(deviceId != 'all' && eventType != ''){
		var eventsLoader = addLoaderMessage('Getting events...');
		jQuery.getJSON('/ajax.php?fname=getevents',{
			start_date 		: fromDateStr,
			end_date 		: toDateStr,
			start_time 		: fromTimeStr,
			end_time 		: toTimeStr,
			device_id		: deviceId
		},function(data){			
			removeLoaderMessage(eventsLoader);
			displayEventDataOnMap(data, eventType);
			showToggle(['events']);
		});		
	};
	
	return false;
};

function getRadioStrengthColor(strength,type){
	strength = parseInt(strength,10);
	var folder = '';
	var file = '';
	switch(type){
		case 'gsm':
			folder = 'images/map_icons_2/dotted/';
			break;
		case 'wcdma':
			folder = 'images/map_icons_2/undotted/';
			break;
	};
	
	if(strength < -120){
		file = '9.png';
		//return 'ED1F24';
	} else if(strength >= -120 && strength < -85){
		file = '7.png';
		//return 'D4632A';
	} else if(strength >= -85 && strength < -50){
		file = '4.png';
		//return 'A99637';
	} else if(strength >= -50){
		file = '1.png';
		//return '69BD45';
	};	
	
	/*
	if(strength < -120){
		file = '9.png';
		//return 'ED1F24';
	} else if(strength >= -120 && strength < -110){
		file = '8.png';
		//return 'E04927';
	} else if(strength >= -110 && strength < -100){
		file = '7.png';
		//return 'D4632A';
	} else if(strength >= -100 && strength < -90){
		file = '6.png';
		//return 'C7762E';
	} else if(strength >= -90 && strength < -80){
		file = '5.png';
		//return 'B98733';
	} else if(strength >= -80 && strength < -70){
		file = '4.png';
		//return 'A99637';
	} else if(strength >= -70 && strength < -60){
		file = '3.png';
		//return '98A43C';
	} else if(strength >= -60 && strength < -50){
		file = '2.png';
		//return '84B040';
	} else if(strength >= -50){
		file = '1.png';
		//return '69BD45';
	};
	
	*/
	
	return folder + file;
};

function displayGSMDataOnMap(data){
	if(!data.length) return false;
	if(gsmMarkers.length){
		jQuery.each(gsmMarkers,function(index,item){
			item.setMap(null);
		});
		gsmMarkers = [];
	};
	var bounds = new google.maps.LatLngBounds();
	jQuery.each(data,function(index,item){
		var latLng = new google.maps.LatLng(item.gsm.gps.latitude,item.gsm.gps.longitude);
		/*var circle = new google.maps.Circle({
			clickable: false,
			fillColor: getRadioStrengthColor(item.gsm.RxL),
			fillOpacity: 0.5,
			strokeWeight: 0,
			center: latLng,
			radius: 60,
			zindex: 5
		});*/
		var marker = new google.maps.Marker({
			position: latLng, 
			title: 'Marker ' + index,
			icon: new google.maps.MarkerImage('/' + getRadioStrengthColor(item.gsm.RxL,'gsm'))
		});
		//gsmMarkers.push(circle);
		
		google.maps.event.addListener(marker,'click',function(){
			showRadioTables(item.gsm.gps_id,item.gsm.device_id,'gsm');
		});
		
		gsmMarkers.push(marker);
		bounds.extend(latLng);
		//circle.setMap(map);
		marker.setMap(map);
	});
	if(data.length){
		if(mapBounds == null){
			mapBounds = bounds;		
		} else {
			mapBounds.union(bounds);		
		};
		map.fitBounds(mapBounds);
	};
};

function displayWCDMADataOnMap(data){
	if(!data.length) return false;
	if(wcdmaMarkers.length){
		jQuery.each(wcdmaMarkers,function(index,item){
			item.setMap(null);
		});
		wcdmaMarkers = [];
	}
	var bounds = new google.maps.LatLngBounds();
	jQuery.each(data,function(index,item){
		var latLng = new google.maps.LatLng(item.wcdma.gps.latitude,item.wcdma.gps.longitude);
		/*var circle = new google.maps.Circle({
			clickable: false,
			fillColor: getRadioStrengthColor(item.wcdma.RSCP),
			strokeColor: getRadioStrengthColor(item.wcdma.RSCP),			
			fillOpacity: 0.5,
			strokeWeight: 5,
			center: latLng,
			radius: 30,
			zindex: 50			
		});*/
		//wcdmaMarkers.push(circle);
		var marker = new google.maps.Marker({
			position: latLng, 
			title: 'Marker ' + index,
			icon: new google.maps.MarkerImage('/' + getRadioStrengthColor(item.wcdma.RSCP, 'wcdma'))
		});
		
		google.maps.event.addListener(marker,'click',function(){
			showRadioTables(item.wcdma.gps_id,item.wcdma.device_id,'wcdma');
		});
		
		wcdmaMarkers.push(marker);
		bounds.extend(latLng);
		marker.setMap(map);
		//circle.setMap(map);
	});
	if(data.length){
		if(mapBounds == null){
			mapBounds = bounds;		
		} else {
			mapBounds.union(bounds);		
		};
		map.fitBounds(mapBounds);
	};
};

function displayGPSDataOnMap(data){
	if(!data.length) return false;
	if(gpsMarkers.length) gpsMarkers = [];
	var bounds = new google.maps.LatLngBounds();
	jQuery.each(data,function(index,item){
		var latLng = new google.maps.LatLng(item.gps.latitude,item.gps.longitude);
		var satTime = getDateObject(item.gps.SatTime);
		var marker = new google.maps.Marker({
			position: latLng,
			title: 'GPS Marker ' + satTime.toUTCString(),
			icon: new google.maps.MarkerImage('/images/gps_icon.png')
		});
		gpsMarkers.push(marker);
		bounds.extend(latLng);
		marker.setMap(map);
	});	
	if(data.length){
		if(mapBounds == null){
			mapBounds = bounds;		
		} else {
			mapBounds.union(bounds);		
		};
		map.fitBounds(mapBounds);
	};
};

function displayEventDataOnMap(data, eventType){
	
	if(!data.length) return false;
	if(eventMarkers.length){
		jQuery.each(eventMarkers,function(index,item){
			item.setMap(null);
		});
		eventMarkers = [];
	};
	var bounds = new google.maps.LatLngBounds();
	var phoneIcon = new google.maps.MarkerImage('/images/map_icons_2/phone2.png');
	var phoneHangupIcon = new google.maps.MarkerImage('/images/map_icons_2/phone2_hangup.png');
	var dataIcon = new google.maps.MarkerImage('/images/map_icons_2/data2.png');
	var ftpIcon = new google.maps.MarkerImage('/images/map_icons_2/data2_ftp.png');
	var smsIcon = new google.maps.MarkerImage('/images/map_icons_2/sms2.png');
	var resizeMap = false;
	jQuery.each(data,function(index,item){	
		var showMarker  = false;
		if(eventType == 'FTP' || eventType == 'HTTP'){
			if(item.event.Type == eventType || item.event.Type == 'DATA'){
				showMarker = true;
			};
		};
		if(eventType == 'VOICE'){
			if(item.event.Type == eventType && (item.event.Title == 'CALLING' || item.event.Title == 'HANGUPCALL')){
				showMarker = true;
			};
		};
		
		if(eventType == 'SMS'){
			if(item.event.Type == eventType){
				showMarker = true;
			};
		};
		
		
		if(showMarker){		
			resizeMap = true;
			var latLng = new google.maps.LatLng(item.event.gps.latitude,item.event.gps.longitude);
			if(item.event.Type == 'VOICE'){
				if(item.event.Title == 'HANGUPCALL'){
					var markerImage = phoneHangupIcon;
				} else if(item.event.Title == 'CALLING'){
					var markerImage = phoneIcon;					
				};				
			} else if(item.event.Type == 'SMS'){
				var markerImage = smsIcon;
			} else if(item.event.Type == 'HTTP' || item.event.Type == 'DATA'){
				var markerImage = dataIcon;
			} else if(item.event.Type == 'FTP'){
				var markerImage = ftpIcon;
			};
			var marker = new google.maps.Marker({
				position: latLng,
				title: '(' + item.event.id + ') ' + item.event.Type + ' - ' + item.event.Title,
				icon: markerImage
			});
			
			eventMarkers.push(marker);
			bounds.extend(latLng);
			marker.setMap(map);				
		};
	});	
	if(resizeMap){
		if(mapBounds == null){
			mapBounds = bounds;		
		} else {
			mapBounds.union(bounds);		
		};
		map.fitBounds(mapBounds);
	};
};


function toggleMarkers(markerSet, visible){
	var thisMarkers;
	switch(markerSet){
		case 'gps':
			thisMarkers = gpsMarkers;
			break;
		case 'gsm':
			thisMarkers = gsmMarkers;
			break;
		case 'wcdma':
			thisMarkers = wcdmaMarkers;
			break;
		case 'events':
			thisMarkers = eventMarkers;
			break;
		default:
			return false;
			break;
	};
	
	if(!thisMarkers.length) return false;
	jQuery.each(thisMarkers,function(index,marker){
		marker.setVisible(visible);
	});
};

function initFleetForm(){
	var fleetForm = jQuery('#fleet_tracker_form');
	if(!fleetForm.length) return false;
	getProbeIds(false);
	var probeField = fleetForm.find('select[name="filter_probe_id"]');
	if(probeField.find('option').length == 1){
		probeField.find('option').attr('selected','selected');
	};
	
	fleetForm.submit(function(){
		return validateFleetForm();
	});

	fleetForm.find('#fleet_form_submit_button').click(function(){
		fleetForm.submit();
		return false;
	});
};

function validateFleetForm(){
	clearInterval(fleetInterval);
	var fleetForm = jQuery('#fleet_tracker_form');
	if(!fleetForm.length) return false;
	var probeField = fleetForm.find('select[name="filter_probe_id"]');
	var probeIds = probeField.val();
	
	//Clear the old markers.
	jQuery.each(fleetMarkers,function(index,item){
		jQuery.each(item,function(index,itemMarker){
			itemMarker.setMap(null);
		});
	});
	fleetMarkers = [];
	
	fleetData.probeIds = probeIds;
	fleetData.data = [];
	var loadedSignal = {};
	var loaderCount = probeIds.length;
	var fleetLoader = addLoaderMessage('Getting fleet information...');
	jQuery.each(probeIds,function(index,item){
		jQuery.getJSON('/ajax.php?fname=getprobefleetdata&probe_id=' + item,function(data){
			loaderCount--;
			if(loaderCount == 0){
				removeLoaderMessage(fleetLoader);
			};
			data.probeId = item;
			data.probeTitle = probeField.find('option[value="' + item + '"]').html();
			putFleetTrackerOnMap(data,true);
			fleetData.data.push(data);			
		});
	});
	
	fleetInterval = setInterval(function(){
		jQuery.each(fleetMarkers,function(index,item){
			jQuery.each(item,function(index,itemMarker){
				itemMarker.setMap(null);
			});
		});
		fleetMarkers = [];
		jQuery.each(fleetData.data,function(index,item){
			putFleetTrackerOnMap(item,false);
		});		
	},(1000*30));
	
	
	return false;
};


function putFleetTrackerOnMap(data,resizeMap){
	if(!data.length) return false;
	var bounds = new google.maps.LatLngBounds();
	var lineData = [];
	var markerData = [];
	
	jQuery.each(data,function(index,item){				
		var latitude = item.gps.latitude;
		var longitude = item.gps.longitude;
		var latLng = new google.maps.LatLng(latitude,longitude);
		lineData.push(latLng);
		
		if(index == 0){
			if(data.probeId == activeFleetMarkerId){
				var markerIcon = new google.maps.MarkerImage('/images/map_icons_2/dotted/9.png')
			} else {
				var markerIcon = new google.maps.MarkerImage('/images/map_icons_2/dotted/10.png')
			};	
			var marker = new google.maps.Marker({ 
				position: latLng, 
				title: 'Tracking Gipsy: ' + data.probeTitle,
				icon: markerIcon
			});
			google.maps.event.addListener(marker,'click',function(){
				showProbeInfo(data);
				changeMarkerIcon(marker);
			});
			marker.probeId = data.probeId;
			marker.setMap(map);
			markerData.push(marker);
		};		
		bounds.extend(latLng);		
	});
	var newLine = new google.maps.Polyline({
		'path'			: lineData,
		'clickable'		: false,
		'map'			: map,
		'strokeColor'	: '123456',
		'strokeOpacity'	: 1,
		'strokeWeight'	: 100
	});
	markerData.push(newLine);
	fleetMarkers.push(markerData);
	if(resizeMap) map.fitBounds(bounds);
};


function showProbeInfo(probeData){
	jQuery('#map_filter_holder #fleet_tracker_form').siblings().remove();
	var probesLoader = addLoaderMessage('Getting Gipsy infomation...');
	jQuery.getJSON('/ajax.php?fname=getprobes',function(data){
		removeLoaderMessage(probesLoader);
		var thisProbe;
		jQuery.each(data,function(index,item){
			if(item.probe.id == probeData.probeId){
				thisProbe = item.probe;
			};
		});
		
		jQuery('#map_filter_holder #fleet_tracker_form').siblings().remove();
		var holder = jQuery('<div />');
		holder.attr('id','fleet_probe_info_holder_' + thisProbe.id).addClass('fleet_probe_info_holder').hide();
		
		var table = jQuery('<table />');
		table.addClass('fleet_probe_info_table bgcolor2 border1').attr({
			'cellpadding': 5,
			'cellspacing': 0
		});
		
		//Add a header
		table.append('<tr class="tableheader"><th colspan="2">GIPSY DETAILS</th></tr>');
		
		//Add the Basic Details
		table.append('<tr class="odd"><td class="label">MAC Address</td><td>' + thisProbe.ProbeMAC + '</td></tr>');
		
		table.append('<tr class="even"><td class="label">Software Version</td><td>' + thisProbe.SoftwareVersion + '</td></tr>');
		
		table.append('<tr class="odd"><td class="label">Model</td><td>' + thisProbe.Model + '</td></tr>');
		
		table.append('<tr class="even"><td class="label">OS Version</td><td>' + thisProbe.OSversion + '</td></tr>');
		
		var devicesRow = jQuery('<tr />');
		devicesRow.addClass('odd');
		var devicesLabel = jQuery('<td class="label">Devices</td>');
		devicesRow.append(devicesLabel);
		
		var devicesField = jQuery('<td />');
		jQuery.each(thisProbe.devices,function(index,item){
			if(index > 0){
				devicesField.append('<div class="devices_field_divider_line"></div>');
			};
			devicesField.append('<strong>MAC Address:</strong> ' + item.DeviceMAC + '<br />');
			devicesField.append('<strong>Make:</strong> ' + item.Make + '<br />');
			devicesField.append('<strong>Model:</strong> ' + item.Model + '<br />');
			devicesField.append('<strong>Firmware Version:</strong> ' + item.FirmwareVersion);			
		});
		
		devicesRow.append(devicesField).appendTo(table);
		table.appendTo(holder);
		
		holder.appendTo(jQuery('#map_filter_holder')).slideDown();	


		//Add the task form.
		holder.after('<div class="clear"></div>');
		var taskSelect = jQuery('<select />');
		taskSelect.attr({
			'name' : 'task_device_id'
		});
		
		taskSelect.append('<option value="">Please select a Device ID</option');
		//taskSelect.append('<option value="all">All Devices</option>');
		jQuery.each(thisProbe.devices,function(index,item){
			taskSelect.append('<option value="' + item.id + '">Device ' + item.id + '</option>');
		});
		
		var taskHolder = jQuery('<div />');
		taskHolder.append('<h4 class="nomargin">Show Tasks</h4>');
		taskHolder.attr('id','task_device_select_holder').append(taskSelect);
		var taskButton = jQuery('<a />');
		taskButton.attr('id','task_device_select_button').addClass('buttonstyle1').html('Show').appendTo(taskHolder);
		taskHolder.append('<div class="clear"></div>');
		
		holder.parent().append(taskHolder).append('<div class="clear"></div>');
		
		//Set click event for task form.
		taskButton.click(function(){
			getTasks(taskSelect.val());
		});
	
		
	});
};


function changeMarkerIcon(marker){
	jQuery.each(fleetMarkers,function(index,item){
		var marker = item[0];
		var markerIcon = new google.maps.MarkerImage('/images/map_icons_2/dotted/10.png');
		marker.setIcon(markerIcon);
	});
	var image = new google.maps.MarkerImage('/images/map_icons_2/dotted/9.png');
	marker.setIcon(image);
	activeFleetMarkerId = marker.probeId;
};

function getTasks(deviceId){
	if(!deviceId.length) return false;
	
	jQuery('#map_section #task_details_holder').remove();
	var tasksLoader = addLoaderMessage('Getting tasks...');
	jQuery.getJSON('/ajax.php?fname=gettasks&device_id=' + deviceId,function(data){
		removeLoaderMessage(tasksLoader);
		var table = jQuery('<table />');
		table.attr({
			'id' : 'task_table',
			'cellpadding' : 5,
			'cellspacing' : 0,
		});
		
		table.addClass('border1').append('<tr class="tableheader"><th>ID</th><th>Type</th><th>Device ID</th><th>Parameter</th><th>Status</th><th>Option</th><th>Created</th><th>Updated</th></tr>');
		
		if(data.length){
		
			jQuery.each(data,function(index,item){
				var task = item.task;
				var newRow = jQuery('<tr />');
				if((index+1) % 2 == 1){
					newRow.addClass('odd');
				} else {
					newRow.addClass('even');
				};
				
				newRow.append('<td>' + task.id + '</td>');
				newRow.append('<td>' + task.Type + '</td>');
				newRow.append('<td>' + task.device_id + '</td>');
				newRow.append('<td>' + task.Parameter + '</td>');
				newRow.append('<td>' + task.Status + '</td>');
				newRow.append('<td>' + task.Option + '</td>');
				
				if(task.created_at.length && task.created_at != 'null'){
					var createdDate = getDateObject(task.created_at);
					var createdDateStr = createdDate.toUTCString();
				} else {
					var createdDateStr = '';
				};
				
				if(task.updated_at.length && task.updated_at != 'null'){
					var updatedDate = getDateObject(task.updated_at);
					var updatedDateStr = updatedDate.toUTCString();
				} else {
					var updatedDateStr = '';
				};
				
				var updatedDate = 
				newRow.append('<td>' + createdDateStr + '</td>');
				newRow.append('<td>' + updatedDateStr + '</td>');
				
				table.append(newRow);
				
			});
		
		} else {			
			table.append('<tr class="tablemessagerow odd"><td class="tablemessage" colspan="8">No Tasks logged for this Device</td></tr>');			
		};
		
		var taskFormHolder = jQuery('<div id="task_form_holder" class="bgcolor2 border1"></div>');
		var taskForm = jQuery('<form id="task_form" name="task_form"></form>');
		
		//Add device input
		
		var deviceMAC = jQuery('select[name="task_device_id"] option[value="' + deviceId + '"]').html();
		
		taskForm.append('<div class="form_leftcol input_holder"><label for="task[device_id]">Choose Device</label><div class="fieldcolumn"><span>' + deviceMAC + '</span><input type="hidden" name="task[device_id]" value="' + deviceId + '" /></div></div>');
		
		//Add type input
		taskForm.append('<div class="form_rightcol input_holder"><label for="task_form_type">Choose Type</label><div class="fieldcolumn"><select name="task[Type]" id="task_form_type"><option value="smstest">SMS</option><option value="calltest">Voice</option><option value="httptest">HTTP</option><option value="ftptest">FTP</option><option value="null">IDLE</option></select></div></div>');
		
		taskForm.append('<div class="clear heightblock2"></div>');
		
		//Add option input
		taskForm.append('<div class="form_leftcol input_holder"><label for="task_form_option">Option</label><div class="fieldcolumn"><input type="text" name="task[Option]" id="task_form_option" /></div></div>');
		
		taskForm.append('<div class="form_rightcol input_holder"><label for="task_form_parameter">Parameter</label><div class="fieldcolumn"><input type="text" name="task[Parameter]" id="task_form_parameter" /></div></div>');
		
		taskForm.append('<input type="hidden" name="task[Status]" type="hidden" value="new" />');
		
		taskForm.append('<div class="clear"></div>');
		taskForm.append('<a id="task_form_submit_button" class="buttonstyle1">Create</a><div class="clear"></div>');
		
		taskForm.appendTo(taskFormHolder);
		taskFormHolder.prepend('<h4 class="nomargin">Add a New Task</h4><div class="clear heightblock2"></div>');		
		
		var holder = jQuery('<div id="task_details_holder"></div>');
		holder.append(table).append(taskFormHolder).append('<div class="clear"></div>').insertAfter(jQuery('#map_section #map_filter_holder'));		
		
		
		taskForm.submit(function(){
			var deviceId = taskForm.find('input[name="task[device_id]"]').val();
			var taskType = taskForm.find('select[name="task[Type]"]').val();
			var taskOption = taskForm.find('input[name="task[Option]"]').val();
			var taskParameter = taskForm.find('input[name="task[Parameter]"]').val();
			
						
			var data = {
				'device_id'		: deviceId,
				'type'			: taskType,
				'option'		: taskOption,
				'parameter'		: taskParameter
			};
			
			submitTaskForm(data);
			return false;
		});
		
		taskForm.find('a#task_form_submit_button').click(function(){
			taskForm.submit();
		});
		
		taskForm.find('input[type="text"]').keypress(function(e){
			if(e.which == 13) taskForm.submit();
		});
	});	
};

function submitTaskForm(formData){
	var taskLoader = addLoaderMessage('Submitting new Task...');
	var taskForm = jQuery('form#task_form');
	taskForm.siblings('.task_form_message').remove();
	if(formData.parameter == ''){
		jQuery('<div class="task_form_message fail_message">Parameter option cannot be left blank.</div>').hide().insertAfter(taskForm).slideDown();
		return false;
	};
	
	jQuery.post('/ajax.php?fname=newtask',formData,function(data){
		if(data.task){
			var successMessage = jQuery('<div class="task_form_message success_message">Your task has been successfully submitted. Please wait a moment while the table reloads.</div>');
			successMessage.hide().insertAfter(taskForm).slideDown();
			setTimeout(function(){
				jQuery('a#task_device_select_button').click();
			},5000);
		} else {
			var failMessage = jQuery('<div class="task_form_message fail_message">There was an error submitting your task. Please try again later.</div>');
			failMessage.hide().insertAfter(taskForm).slideDown();
		};
		removeLoaderMessage(taskLoader);
	},'json');
	
	
};


function showToggle(toggles){
	if(!toggles.length) return false;
	var holder = jQuery('#toggle_buttons_holder');
	jQuery.each(toggles,function(index,item){
		var thisMarkers;
		var buttonTitle;
		switch(item){
			case 'gsm':
				thisMarkers = gsmMarkers;
				buttonTitle = 'Show/Hide GSM Markers';
				break;
			case 'wcdma':
				thisMarkers = wcdmaMarkers;
				buttonTitle = 'Show/Hide WCDMA Markers';
				break;
			case 'gps':
				thisMarkers = gpsMarkers;
				buttonTitle = 'Show/Hide GPS Markers';
				break;
			case 'events':
				thisMarkers = eventMarkers;
				buttonTitle = 'Show/Hide Event Markers';
				break;
			default:
				return false;
				break;
		};
		if(!thisMarkers.length) return false;
		
		var subholder = holder.find('.button_' + item);
		if(subholder.find('a').length) return false;
		var button = jQuery('<a class="toggle_button_button buttonstyle1">' + buttonTitle + '</a>');
		var firstMarker = thisMarkers[0];
		if(firstMarker.getVisible()){ button.addClass('active_layer'); };
		
		button.click(function(){
			if(button.hasClass('active_layer')){
				toggleMarkers(item,false);
				button.removeClass('active_layer').addClass('inactive_button_1');
			} else {
				toggleMarkers(item,true);
				button.addClass('active_layer').removeClass('inactive_button_1');
			};
		});
		
		subholder.append(button).removeClass('no_button');
	});
};

function initAccordion(){
	var toggles = jQuery('.accordion_toggle');
	if(!toggles.length) return false;
	toggles.each(function(index,item){
		var toggle = jQuery(item);
		var container = jQuery('.accordion_container[rel="' + toggle.attr('rel') + '"]');
		if(container.length){
			if(!toggle.hasClass('toggle_active')){
				container.hide();
			};
		};
	});

	bindAccordion();	
};

function bindAccordion(){
	var toggles = jQuery('.accordion_toggle');
	toggles.unbind('click.accordion').bind('click.accordion',function(){
		var toggle = jQuery(this);		
		var container = jQuery('.accordion_container[rel="' + toggle.attr('rel') + '"]');
		if(!toggle.hasClass('toggle_active')){			
			var otherToggles = toggle.siblings('.accordion_toggle');
			var otherContainers = container.siblings('.accordion_container');
			otherToggles.removeClass('toggle_active').addClass('toggle_inactive');
			otherContainers.removeClass('container_active').addClass('toggle_inactive').slideUp().trigger('container_close');	
			toggle.removeClass('toggle_inactive').addClass('toggle_active');
			container.removeClass('container_inactive').removeClass('container_inactive').addClass('container_active').slideDown().trigger('container_open');	
			
			//jQuery('.accordion_toggle.toggle_active').removeClass('toggle_active').addClass('toggle_inactive');
			//jQuery('.accordion_container.container_active').removeClass('container_active').addClass('container_inactive').slideUp().trigger('container_close');
					
		};		
		return false;
	});	
}

function renderChart(){
	var holder = jQuery('#chart_holder');
	if(!holder.length) return false;
	var date = new Date();
	var month = date.getMonth()+1;
	var year = date.getFullYear();
	
	//Get the past twelve months.
	var times = [];
	var shortMonths = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var longMonths = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	var timeMonth = month.toString();
	if(timeMonth.length == 1) timeMonth = '0' + timeMonth;
	var timeYear = year.toString().substr(2,2);
	var timeCode = timeYear + timeMonth;
	times.push({ 'year' : year, 'month' : month, 'shortMonth' : shortMonths[month-1], 'longMonth' : longMonths[month-1], 'timeCode' : timeCode });
	var newMonth = month;
	var newYear = year;	
	for(i = 1; i <= 11; i++){
		newMonth--;
		if(newMonth < 1){
			newMonth = 12;
			newYear--;
		};
		
		timeMonth = newMonth.toString();
		if(timeMonth.length == 1) timeMonth = '0' + timeMonth;
		timeYear = newYear.toString().substr(2,2);
		timeCode = timeYear + timeMonth;
		
		times.push({ 'year': newYear, 'month' : newMonth, 'shortMonth' : shortMonths[newMonth-1], 'longMonth' : longMonths[newMonth-1], 'timeCode': timeCode });
	};
	
	var scale = jQuery('<ul id="chart_scale"></ul>');	
	var timeline = jQuery('<ul id="chart_timeline"></ul>');
	
	jQuery('#chart').remove();
	
	var chart = jQuery('<div id="chart"></div>');
	
	jQuery.each(times,function(index,time){
		timeline.prepend('<li><a href="#" rel="' + time.timeCode + '">' + time.shortMonth + '</a></li>');
		var newBlock = jQuery('<div class="chart_block chart_block_loading" rel="' + time.timeCode + '"></div>');
		newBlock.prependTo(chart);
	});
	
	//holder.append(scale);
	
	holder.append(timeline);
	holder.append(chart);
	
	
	var chartLoader = addLoaderMessage('Getting chart...');
	var loadCount = times.length;
	jQuery.each(times,function(index,item){		
		jQuery.getJSON('/ajax.php?fname=getchart',{
			'm' : item.month,
			'y' : item.year
		},function(data){
			loadCount--;
			if(loadCount == 0) removeLoaderMessage(chartLoader);
			
			var chartBlock = jQuery('#chart').find('.chart_block[rel="' + item.timeCode + '"]');
						
			var eventCount = data.count.eventcount;
			var gsmCount = data.count.gsmcount;
			var wcdmaCount = data.count.wcdmacount;
			
			var eventTotal = 0;
			var gsmTotal = 0;
			var wcdmaTotal = 0;
			
			
			jQuery.each(eventCount,function(index,item){
				eventTotal += item.event.count;
			});
			
			jQuery.each(gsmCount,function(index,item){
				gsmTotal += item.gsm.count;
			});
			
			jQuery.each(wcdmaCount,function(index,item){
				wcdmaTotal += item.wcdma.count;
			});
			
					
			chartBlock.append('<div class="chart_bar chart_bar_gsm" style="height: 0px;" rel="' + gsmTotal + '" title="GSM Records in ' + item.longMonth + ' ' + item.year + ':<br /> <strong>' + gsmTotal + '</strong>"></div>');
			chartBlock.append('<div class="chart_bar chart_bar_wcdma" style="height: 0px;" rel="' + wcdmaTotal + '" title="WCDMA Records in ' + item.longMonth + ' ' + item.year + ': <br /> <strong>' + wcdmaTotal + '</strong>"></div>');
			chartBlock.append('<div class="chart_bar chart_bar_events" style="height: 0px;" rel="' + eventTotal + '" title="Events Records in ' + item.longMonth + ' ' + item.year + ': <br /> <strong>' + eventTotal + '</strong>"></div>');
			
			chartBlock.removeClass('chart_block_loading');
			//console.log(gsmTotal + '------' + wcdmaTotal + '-------' + eventTotal);
			if(loadCount == 0) renderChartScale();		
			
		});
	});	
};

function renderChartScale(){
	var holder = jQuery('#chart_holder');
	var scale = holder.find('#chart_scale');
	var bars = holder.find('.chart_bar');
	var grandTotal = 0;
	jQuery.each(bars,function(index,item){
		var bar = jQuery(item);
		var barCount = parseInt(bar.attr('rel'),10);
		if(barCount > grandTotal) grandTotal = barCount;		
	});	
	
	var scaleCount = 5;
	scale.find('li').remove();	
	for(i=scaleCount;i>=1;i--){
		var segmentValue = Math.round((grandTotal/scaleCount)*i);
		scale.append('<li>' + segmentValue + '</li>');
	};
	scale.append('<li class="zerobase">0</li>');
	
	jQuery.each(bars,function(index,item){
		var bar = jQuery(item);
		var barCount = parseInt(bar.attr('rel'),10);
		var percentage = Math.round((barCount / grandTotal)*100);
		bar.css('height',percentage + '%');		
	});
	
	bars.click(function(){
		var bar = jQuery(this);
		var dataType = '';
		if(bar.hasClass('chart_bar_events')){
			dataType = 'events';
		} else if(bar.hasClass('chart_bar_gsm')){
			dataType = 'gsm';
		} else if(bar.hasClass('chart_bar_wcdma')){
			dataType = 'wcdma';
		};
		var timeCode = bar.parent().attr('rel');
		var timeYear = parseInt(timeCode.substr(0,2),10);
		var timeMonth = parseInt(timeCode.substr(2,2),10);
		
		if(timeYear > 50){
			timeYear = '19' + timeYear.toString();
		} else {
			timeYear = '20' + timeYear.toString();
		};
		
		if(timeMonth == 1 || timeMonth == 3 || timeMonth == 5 || timeMonth == 7 || timeMonth == 8 || timeMonth == 10 || timeMonth == 12){
			var lastDay = '31';
		} else if(timeMonth == 4 || timeMonth == 6 || timeMonth == 9 || timeMonth == 11){
			var lastDay = '30';
		} else if(timeMonth == 2){
			var timeYearInt = parseInt(timeYear,10);
			if(timeYearInt % 4 == 0){
				var lastDay = '29';
			} else {
				var lastDay = '28';
			};
		};
		
		var firstDay = '01';
		
		if(timeMonth < 10){
			timeMonth = '0' + timeMonth.toString();
		};
		
		jQuery('input[name="filter_date_range_start"]').val(firstDay + '/' + timeMonth + '/' + timeYear);
		jQuery('input[name="filter_date_range_end"]').val(lastDay + '/' + timeMonth + '/' + timeYear);
		
		
		var radioButtons = jQuery('input[name="filter_technology"]');
		radioButtons.removeAttr('checked');
		if(dataType == 'gsm' || dataType == 'wcdma'){
			jQuery('input[name="filter_technology"][value="' + dataType + '"]').attr('checked','checked');		
		} else {
			jQuery('input[name="filter_technology"][value="all"]').attr('checked','checked');
		};
		
		jQuery('.accordion_toggle[rel="map"]').click();
		
	});
	
	bars.qtip({
		position: {
			target: 'mouse'
		}
	});
			
	
	jQuery('#chart_timeline li a').click(function(){
	
		var timeCode = jQuery(this).attr('rel');
		var timeYear = parseInt(timeCode.substr(0,2),10);
		var timeMonth = parseInt(timeCode.substr(2,2),10);
		
		if(timeYear > 50){
			timeYear = '19' + timeYear.toString();
		} else {
			timeYear = '20' + timeYear.toString();
		};
		
		if(timeMonth == 1 || timeMonth == 3 || timeMonth == 5 || timeMonth == 7 || timeMonth == 8 || timeMonth == 10 || timeMonth == 12){
			var lastDay = '31';
		} else if(timeMonth == 4 || timeMonth == 6 || timeMonth == 9 || timeMonth == 11){
			var lastDay = '30';
		} else if(timeMonth == 2){
			var timeYearInt = parseInt(timeYear,10);
			if(timeYearInt % 4 == 0){
				var lastDay = '29';
			} else {
				var lastDay = '28';
			};
		};
		
		var firstDay = '01';
		
		if(timeMonth < 10){
			timeMonth = '0' + timeMonth.toString();
		};
		
		jQuery('input[name="filter_date_range_start"]').val(firstDay + '/' + timeMonth + '/' + timeYear);
		jQuery('input[name="filter_date_range_end"]').val(lastDay + '/' + timeMonth + '/' + timeYear);
		
		var radioButtons = jQuery('input[name="filter_technology"]');
		radioButtons.removeAttr('checked');
		jQuery('input[name="filter_technology"][value="all"]').attr('checked','checked');	
		
		
	
		jQuery('.accordion_toggle[rel="map"]').click();
	
	});
	
};

function getDateObject(dateStr){
	if(!dateStr.length) return new Date();
	var fields = dateStr.replace('Z','').split('T');
	var fields1 = fields[0].split('-');
	var fields2 = fields[1].split(':');
	var dateObj = {
		'year': fields1[0],
		'month' : (parseInt(fields1[1],10)-1),
		'day': fields1[2],
		'hours' : fields2[0],
		'minutes' : fields2[1],
		'seconds' : fields2[2]
	};

	return new Date(dateObj.year, dateObj.month, dateObj.day, dateObj.hours, dateObj.minutes, dateObj.seconds);	
	
};


function showRadioTables(gpsId,deviceId,type){
	if(type != 'gsm' && type != 'wcdma') return false;
	var holder = jQuery('.accordion_container[rel="map"]');
	holder.find('.radio_data_table_holder, .radio_data_clear').remove(); //Clear the old tables.
	
	if(type == 'gsm'){
		holder.append('<div id="radio_data_table_holder_gsm" class="radio_data_table_holder radio_data_loading radio_data_2_cols alpha border1 bgcolor2"></div>');
		holder.append('<div id="radio_data_table_holder_g_neighbours" class="radio_data_table_holder radio_data_loading radio_data_2_cols omega border1 bgcolor2"></div>');
		holder.append('<div class="clear radio_data_clear"></div>');
		
		
		//Get GSM data.
		var gsmdataLoader = addLoaderMessage('Getting GSM data...');
		jQuery.getJSON('/ajax.php?fname=getgsmdata&gps_id=' + gpsId + '&device_id=' + deviceId,function(data){
			removeLoaderMessage(gsmdataLoader);			
			var holder = jQuery('#radio_data_table_holder_gsm');
			holder.append('<div class="radio_data_table_header">GSM Data</div>');
			holder.removeClass('radio_data_loading');
			if(!data.length){				
				var message = jQuery('<p class="no-radio-data-message bgcolor1">No Data Available</p>');
				message.hide().appendTo(holder).slideDown();				
			} else {
				if(data.length == 1){
					var gitem = data[0].gsm;
					var keys = {
						'BSIC'		: 'BSIC',
						'CellId'	: 'Cell ID',
						'Ch'		: 'Ch',
						'LAC'		: 'LAC',
						'MCC'		: 'MCC',
						'MNC'		: 'MNC',
						'Mode'		: 'Mode',
						'RxL'		: 'RxL',
						'Type'		: 'Type',
						'device_id'	: 'Device ID',
						'epochtime'	: 'Timestamp',
						'gps_id'	: 'GPS ID',
						'id'		: 'ID',
						'nanotime'	: 'Nanotime'
					};
					
					var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">GSM</th></tr></table>');
					var iCount = 1;
					for(var key in keys){
						if(iCount%2 == 1){
							var classname = 'odd';
						} else {
							var classname = 'even';
						};
						
						table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + gitem[key] + '</td></tr>');
						
						iCount++;
					};
					
					table.appendTo(holder);
					
					
					
				} else if (data.length > 1){
				
					jQuery.each(data, function(index, item){
						var toggleButton = jQuery('<a class="data_table_accordion_header accordion_toggle" rel="gsm_data_' + index + '">Data Object</a>');
						if(index == 0){
							toggleButton.addClass('toggle_active');
						} else {
							toggleButton.addClass('toggle_inactive');
						};
												
						var gitem = item.gsm;
						var keys = {
							'BSIC'		: 'BSIC',
							'CellId'	: 'Cell ID',
							'Ch'		: 'Ch',
							'LAC'		: 'LAC',
							'MCC'		: 'MCC',
							'MNC'		: 'MNC',
							'Mode'		: 'Mode',
							'RxL'		: 'RxL',
							'Type'		: 'Type',
							'device_id'	: 'Device ID',
							'epochtime'	: 'Timestamp',
							'gps_id'	: 'GPS ID',
							'id'		: 'ID',
							'nanotime'	: 'Nanotime'					
						};
						
						var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">GSM</th></tr></table>');
						var iCount = 1;
						for(var key in keys){
							if(iCount%2 == 1){
								var classname = 'odd';
							} else {
								var classname = 'even';
							};
							
							table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + gitem[key] + '</td></tr>');
							
							iCount++;
						};
						
						var container = jQuery('<div class="accordion_container" rel="gsm_data_' + index + '"></div>');
						
						if(index == 0){
							container.addClass('container_active');
						} else {
							container.addClass('container_inactive').hide();
						};
						
						toggleButton.appendTo(holder);
						container.append(table).appendTo(holder);
					});
					
					bindAccordion();
					//holder.find('accordion_toggle.toggle_active').click();
				};
			};
			
			
			
		});
		
		
		//Get g_neighbour data.
		var gneighbourLoader = addLoaderMessage('Getting G Neighbour data...');
		jQuery.getJSON('/ajax.php?fname=getgneighbourdata&gps_id=' + gpsId + '&device_id=' + deviceId,function(data){
			removeLoaderMessage(gneighbourLoader);			
			var holder = jQuery('#radio_data_table_holder_g_neighbours');
			holder.append('<div class="radio_data_table_header">G Neighbours</div>');
			holder.removeClass('radio_data_loading');
			if(!data.length){				
				var message = jQuery('<p class="no-radio-data-message bgcolor1">No Data Available</p>');
				message.hide().appendTo(holder).slideDown();				
			} else {
				if(data.length == 1){
					var gitem = data[0].g_neighbour;
					var keys = {
						'BSIC'		: 'BSIC',
						'C1'		: 'C1',
						'C2'		: 'C2',
						'CellId'	: 'Cell ID',
						'Ch'		: 'Ch',
						'LAC'		: 'LAC',
						'MCC'		: 'MCC',
						'MNC'		: 'MNC',
						'RxL'		: 'RxL',
						'device_id'	: 'Device ID',
						'epochtime'	: 'Timestamp',
						'gps_id'	: 'GPS ID',
						'id'		: 'ID',
						'nanotime'	: 'Nanotime'
					};
					
					var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">G Neighbour</th></tr></table>');
					var iCount = 1;
					for(var key in keys){
						if(iCount%2 == 1){
							var classname = 'odd';
						} else {
							var classname = 'even';
						};
						
						table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + gitem[key] + '</td></tr>');
						
						iCount++;
					};
					
					table.appendTo(holder);
					
					
					
				} else if (data.length > 1){
				
					jQuery.each(data, function(index, item){
						var toggleButton = jQuery('<a class="data_table_accordion_header accordion_toggle" rel="g_neighbour_' + index + '">Data Object</a>');
						if(index == 0){
							toggleButton.addClass('toggle_active');
						} else {
							toggleButton.addClass('toggle_inactive');
						};
												
						var gitem = item.g_neighbour;
						var keys = {
							'BSIC'		: 'BSIC',
							'C1'		: 'C1',
							'C2'		: 'C2',
							'CellId'	: 'Cell ID',
							'Ch'		: 'Ch',
							'LAC'		: 'LAC',
							'MCC'		: 'MCC',
							'MNC'		: 'MNC',
							'RxL'		: 'RxL',
							'device_id'	: 'Device ID',
							'epochtime'	: 'Timestamp',
							'gps_id'	: 'GPS ID',
							'id'		: 'ID',
							'nanotime'	: 'Nanotime'					
						};
						
						var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">G Neighbour</th></tr></table>');
						var iCount = 1;
						for(var key in keys){
							if(iCount%2 == 1){
								var classname = 'odd';
							} else {
								var classname = 'even';
							};
							
							table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + gitem[key] + '</td></tr>');
							
							iCount++;
						};
						
						var container = jQuery('<div class="accordion_container" rel="g_neighbour_' + index + '"></div>');
						
						if(index == 0){
							container.addClass('container_active');
						} else {
							container.addClass('container_inactive').hide();
						};
						
						toggleButton.appendTo(holder);
						container.append(table).appendTo(holder);
					});
					
					bindAccordion();
					//holder.find('accordion_toggle.toggle_active').click();
				};
			};
			
			
			
			
		});
		
	
	} else if(type == 'wcdma'){
		
		holder.append('<div id="radio_data_table_holder_wcdma" class="radio_data_table_holder radio_data_loading radio_data_2_cols alpha border1 bgcolor2"></div>');
		holder.append('<div id="radio_data_table_holder_w_monitored_inter_freq" class="radio_data_table_holder radio_data_loading radio_data_2_cols omega border1 bgcolor2"></div>');
		holder.append('<div class="clear radio_data_clear"></div>');
		holder.append('<div id="radio_data_table_holder_w_monitored_intra_freq" class="radio_data_table_holder radio_data_loading radio_data_3_cols alpha border1 bgcolor2"></div>');
		holder.append('<div id="radio_data_table_holder_w_monitored_inter_rat" class="radio_data_table_holder radio_data_loading radio_data_3_cols border1 bgcolor2"></div>');
		holder.append('<div id="radio_data_table_holder_w_detected" class="radio_data_table_holder radio_data_loading radio_data_3_cols omega border1 bgcolor2"></div>');		
		holder.append('<div class="clear radio_data_clear"></div>');
		
		//Get WCMDA data.
		var wcdmadataLoader = addLoaderMessage('Getting WCDMA data...');
		jQuery.getJSON('/ajax.php?fname=getwcdmadata&gps_id=' + gpsId + '&device_id=' + deviceId,function(data){
			removeLoaderMessage(wcdmadataLoader);
			var holder = jQuery('#radio_data_table_holder_wcdma');
			holder.append('<div class="radio_data_table_header">WCDMA Data</div>');
			holder.removeClass('radio_data_loading');
			if(!data.length){				
				var message = jQuery('<p class="no-radio-data-message bgcolor1">No Data Available</p>');
				message.hide().appendTo(holder).slideDown();				
			} else {
				if(data.length == 1){
					var witem = data[0].wcdma;
					var keys = {
						'Ch' 		: 'Ch',
						'EcNo' 		: 'EcNo',
						'Hs'		: 'Hs',
						'LAC'		: 'LAC',
						'MCC'		: 'MCC',			
						'MNC'		: 'MNC',
						'Mode'		: 'Mode',						
						'RSCP'		: 'RSCP',
						'RSSI'		: 'RSSI',
						'Rs'		: 'Rs',
						'SC'		: 'SC',
						'ServL'		: 'ServL',
						'ServQ'		: 'ServQ',
						'Type'		: 'Type',
						'device_id'	: 'Device ID',
						'epochtime'	: 'Timestamp',
						'gps_id'	: 'GPS ID',
						'id'		: 'ID',
						'nanotime'	: 'Nanotime'
					};
					
					var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">WCDMA</th></tr></table>');
					var iCount = 1;
					for(var key in keys){
						if(iCount%2 == 1){
							var classname = 'odd';
						} else {
							var classname = 'even';
						};
						
						table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + witem[key] + '</td></tr>');
						
						iCount++;
					};
					
					table.appendTo(holder);
					
					
					
				} else if (data.length > 1){
				
					jQuery.each(data, function(index, item){
						var toggleButton = jQuery('<a class="data_table_accordion_header accordion_toggle" rel="wcdma_' + index + '">Data Object</a>');
						if(index == 0){
							toggleButton.addClass('toggle_active');
						} else {
							toggleButton.addClass('toggle_inactive');
						};
												
						var witem = item.wcdma;
						var keys = {
							'Ch' 		: 'Ch',
							'EcNo' 		: 'EcNo',
							'Hs'		: 'Hs',
							'LAC'		: 'LAC',
							'MCC'		: 'MCC',			
							'MNC'		: 'MNC',
							'Mode'		: 'Mode',						
							'RSCP'		: 'RSCP',
							'RSSI'		: 'RSSI',
							'Rs'		: 'Rs',
							'SC'		: 'SC',
							'ServL'		: 'ServL',
							'ServQ'		: 'ServQ',
							'Type'		: 'Type',
							'device_id'	: 'Device ID',
							'epochtime'	: 'Timestamp',
							'gps_id'	: 'GPS ID',
							'id'		: 'ID',
							'nanotime'	: 'Nanotime'					
						};
						
						var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">WCDMA</th></tr></table>');
						var iCount = 1;
						for(var key in keys){
							if(iCount%2 == 1){
								var classname = 'odd';
							} else {
								var classname = 'even';
							};
							
							table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + witem[key] + '</td></tr>');
							
							iCount++;
						};
						
						var container = jQuery('<div class="accordion_container" rel="wcdma_' + index + '"></div>');
						
						if(index == 0){
							container.addClass('container_active');
						} else {
							container.addClass('container_inactive').hide();
						};
						
						toggleButton.appendTo(holder);
						container.append(table).appendTo(holder);
					});
					
					bindAccordion();
					//holder.find('accordion_toggle.toggle_active').click();
				};
			};			
		});
		
		
		//Get W Monitored Inter Frequencies Data
		var wmonitoredinterfreqdata = addLoaderMessage('Getting W Monitored Inter Frequencies data...');
		jQuery.getJSON('/ajax.php?fname=getwmonitoredinterfreqdata&gps_id=' + gpsId + '&device_id=' + deviceId,function(data){
			removeLoaderMessage(wmonitoredinterfreqdata);			
			var holder = jQuery('#radio_data_table_holder_w_monitored_inter_freq');
			holder.append('<div class="radio_data_table_header">W Monitored Inter Freq</div>');
			holder.removeClass('radio_data_loading');
			if(!data.length){				
				var message = jQuery('<p class="no-radio-data-message bgcolor1">No Data Available</p>');
				message.hide().appendTo(holder).slideDown();				
			} else {
				if(data.length == 1){
					var witem = data[0].w_monitored_inter_freq;
					var keys = {
						'Ch' 		: 'Ch',
						'EcNo' 		: 'EcNo',
						'Hn'		: 'Hn',
						'PTxPwr'	: 'PtxPwr',
						'RSCP'		: 'RSCP',
						'Rn'		: 'Rn',
						'SC'		: 'SC',
						'ServL'		: 'ServL',
						'ServQ'		: 'ServQ',
						'ULTxPwr'	: 'ULTXPwr',
						'device_id'	: 'Device ID',
						'epochtime'	: 'Timestamp',
						'gps_id'	: 'GPS ID',
						'id'		: 'ID',
						'nanotime'	: 'Nanotime'
					};
					
					var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">W Monitored Inter Freq</th></tr></table>');
					var iCount = 1;
					for(var key in keys){
						if(iCount%2 == 1){
							var classname = 'odd';
						} else {
							var classname = 'even';
						};
						
						table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + witem[key] + '</td></tr>');
						
						iCount++;
					};
					
					table.appendTo(holder);
					
					
					
				} else if (data.length > 1){
				
					jQuery.each(data, function(index, item){
						var toggleButton = jQuery('<a class="data_table_accordion_header accordion_toggle" rel="w_monitored_inter_freq_' + index + '">Data Object</a>');
						if(index == 0){
							toggleButton.addClass('toggle_active');
						} else {
							toggleButton.addClass('toggle_inactive');
						};
												
						var witem = item.w_monitored_inter_freq;
						var keys = {
							'Ch' 		: 'Ch',
							'EcNo' 		: 'EcNo',
							'Hn'		: 'Hn',
							'PTxPwr'	: 'PtxPwr',
							'RSCP'		: 'RSCP',
							'Rn'		: 'Rn',
							'SC'		: 'SC',
							'ServL'		: 'ServL',
							'ServQ'		: 'ServQ',
							'ULTxPwr'	: 'ULTXPwr',
							'device_id'	: 'Device ID',
							'epochtime'	: 'Timestamp',
							'gps_id'	: 'GPS ID',
							'id'		: 'ID',
							'nanotime'	: 'Nanotime'					
						};
						
						var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">W Monitored Inter Freq</th></tr></table>');
						var iCount = 1;
						for(var key in keys){
							if(iCount%2 == 1){
								var classname = 'odd';
							} else {
								var classname = 'even';
							};
							
							table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + witem[key] + '</td></tr>');
							
							iCount++;
						};
						
						var container = jQuery('<div class="accordion_container" rel="w_monitored_inter_freq_' + index + '"></div>');
						
						if(index == 0){
							container.addClass('container_active');
						} else {
							container.addClass('container_inactive').hide();
						};
						
						toggleButton.appendTo(holder);
						container.append(table).appendTo(holder);
					});
					
					bindAccordion();
					//holder.find('accordion_toggle.toggle_active').click();
				};
			};
			
			
			
			
		});		
		
		//Get W Monitored Intra Frequencies Data
		var wmonitoredintrafreqdata = addLoaderMessage('Getting W Monitored Intra Frequencies data');
		jQuery.getJSON('/ajax.php?fname=getwmonitoredintrafreqdata&gps_id=' + gpsId + '&device_id=' + deviceId,function(data){
			removeLoaderMessage(wmonitoredintrafreqdata);
			var holder = jQuery('#radio_data_table_holder_w_monitored_intra_freq');
			holder.append('<div class="radio_data_table_header">W Monitored Intra Freq</div>');
			holder.removeClass('radio_data_loading');
			if(!data.length){				
				var message = jQuery('<p class="no-radio-data-message bgcolor1">No Data Available</p>');
				message.hide().appendTo(holder).slideDown();				
			} else {
				if(data.length == 1){
					var witem = data[0];
					var keys = {
						'Ch' 		: 'Ch',
						'EcNo' 		: 'EcNo',
						'Hn'		: 'Hn',
						'PTxPwr'	: 'PtxPwr',
						'RSCP'		: 'RSCP',
						'Rn'		: 'Rn',
						'SC'		: 'SC',
						'ServL'		: 'ServL',
						'ServQ'		: 'ServQ',
						'ULTxPwr'	: 'ULTXPwr',
						'device_id'	: 'Device ID',
						'epochtime'	: 'Timestamp',
						'gps_id'	: 'GPS ID',
						'id'		: 'ID',
						'nanotime'	: 'Nanotime'						
					};
					
					var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">W Monitored Intra Freq</th></tr></table>');
					var iCount = 1;					
					for(var key in keys){
						console.log(key);
						if(iCount%2 == 1){
							var classname = 'odd';
						} else {
							var classname = 'even';
						};
						
						table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + witem[key] + '</td></tr>');
						
						iCount++;
					};
					
					table.appendTo(holder);
					
					
					
				} else if (data.length > 1){
				
					jQuery.each(data, function(index, item){
						var toggleButton = jQuery('<a class="data_table_accordion_header accordion_toggle" rel="wmonitoredintrafreq_' + index + '">Data Object</a>');
						if(index == 0){
							toggleButton.addClass('toggle_active');
						} else {
							toggleButton.addClass('toggle_inactive');
						};
												
						var witem = item.w_monitored_intra_freq;
						var keys = {
							'Ch' 		: 'Ch',
							'EcNo' 		: 'EcNo',
							'Hn'		: 'Hn',
							'PTxPwr'	: 'PtxPwr',
							'RSCP'		: 'RSCP',
							'Rn'		: 'Rn',
							'SC'		: 'SC',
							'ServL'		: 'ServL',
							'ServQ'		: 'ServQ',
							'ULTxPwr'	: 'ULTXPwr',
							'device_id'	: 'Device ID',
							'epochtime'	: 'Timestamp',
							'gps_id'	: 'GPS ID',
							'id'		: 'ID',
							'nanotime'	: 'Nanotime'						
						};
						
						var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">W Monitored Intra Freq</th></tr></table>');
						var iCount = 1;
						for(var key in keys){
							if(iCount%2 == 1){
								var classname = 'odd';
							} else {
								var classname = 'even';
							};
							
							table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + witem[key] + '</td></tr>');
							
							iCount++;
						};
						
						var container = jQuery('<div class="accordion_container" rel="wmonitoredintrafreq_' + index + '"></div>');
						
						if(index == 0){
							container.addClass('container_active');
						} else {
							container.addClass('container_inactive').hide();
						};
						
						toggleButton.appendTo(holder);
						container.append(table).appendTo(holder);
					});
					
					bindAccordion();
					//holder.find('accordion_toggle.toggle_active').click();
				};
			};
		});		
		
		//Get W Monitored Inter Rat Data
		var wmonitoredinterratdataLoader = addLoaderMessage('Getting W Monitored Inter Rat data...');
		jQuery.getJSON('/ajax.php?fname=getwmonitoredinterratdata&gps_id=' + gpsId + '&device_id=' + deviceId,function(data){
			removeLoaderMessage(wmonitoredinterratdataLoader);			
			var holder = jQuery('#radio_data_table_holder_w_monitored_inter_rat');
			holder.append('<div class="radio_data_table_header">W Monitored Inter Rat</div>');
			holder.removeClass('radio_data_loading');
			if(!data.length){				
				var message = jQuery('<p class="no-radio-data-message bgcolor1">No Data Available</p>');
				message.hide().appendTo(holder).slideDown();				
			} else {
				if(data.length == 1){
					var witem = data[0].w_monitored_inter_rat;
					var keys = {
						'BSIC'		: 'BSIC',
						'CIO'		: 'CIO',
						'Ch'		: 'Ch',
						'RSSI'		: 'RSSI',
						'RxLevMin'	: 'RxLevMin',
						'device_id'	: 'Device ID',
						'epochtime'	: 'Timestamp',
						'gps_id'	: 'GPS ID',
						'id'		: 'ID',
						'nanotime'	: 'Nanotime'
					};
					
					var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">W Monitored Inter Rat</th></tr></table>');
					var iCount = 1;
					for(var key in keys){
						if(iCount%2 == 1){
							var classname = 'odd';
						} else {
							var classname = 'even';
						};
						
						table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + witem[key] + '</td></tr>');
						
						iCount++;
					};
					
					table.appendTo(holder);
					
					
					
				} else if (data.length > 1){
				
					jQuery.each(data, function(index, item){
						var toggleButton = jQuery('<a class="data_table_accordion_header accordion_toggle" rel="w_monitored_inter_rat_' + index + '">Data Object</a>');
						if(index == 0){
							toggleButton.addClass('toggle_active');
						} else {
							toggleButton.addClass('toggle_inactive');
						};
												
						var witem = item.w_monitored_inter_rat;
						var keys = {
							'BSIC'		: 'BSIC',
							'CIO'		: 'CIO',
							'Ch'		: 'Ch',
							'RSSI'		: 'RSSI',
							'RxLevMin'	: 'RxLevMin',
							'device_id'	: 'Device ID',
							'epochtime'	: 'Timestamp',
							'gps_id'	: 'GPS ID',
							'id'		: 'ID',
							'nanotime'	: 'Nanotime'
						};
						
						var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">W Monitored Inter Rat</th></tr></table>');
						var iCount = 1;
						for(var key in keys){
							if(iCount%2 == 1){
								var classname = 'odd';
							} else {
								var classname = 'even';
							};
							
							table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + witem[key] + '</td></tr>');
							
							iCount++;
						};
						
						var container = jQuery('<div class="accordion_container" rel="w_monitored_inter_rat_' + index + '"></div>');
						
						if(index == 0){
							container.addClass('container_active');
						} else {
							container.addClass('container_inactive').hide();
						};
						
						toggleButton.appendTo(holder);
						container.append(table).appendTo(holder);
					});
					
					bindAccordion();
					//holder.find('accordion_toggle.toggle_active').click();
				};
			};
			
			
		});
		
		//Get W Detected Data
		var wdetectedLoader = addLoaderMessage('Getting W Detected data...');
		jQuery.getJSON('/ajax.php?fname=getwdetecteddata&gps_id=' + gpsId + '&device_id=' + deviceId,function(data){
			removeLoaderMessage(wdetectedLoader);			
			var holder = jQuery('#radio_data_table_holder_w_detected');
			holder.append('<div class="radio_data_table_header">W Detected</div>');
			holder.removeClass('radio_data_loading');
			if(!data.length){				
				var message = jQuery('<p class="no-radio-data-message bgcolor1">No Data Available</p>');
				message.hide().appendTo(holder).slideDown();				
			} else {
				if(data.length == 1){
					var witem = data[0].w_detected;
					var keys = {
						'Ch' 		: 'Ch',
						'Hn'		: 'Hn',
						'PTxPwr'	: 'PtxPwr',
						'RSCP'		: 'RSCP',
						'Rn'		: 'Rn',
						'SC'		: 'SC',
						'ServL'		: 'ServL',
						'ServQ'		: 'ServQ',
						'ULTxPwr'	: 'ULTXPwr',
						'device_id'	: 'Device ID',
						'epochtime'	: 'Timestamp',
						'gps_id'	: 'GPS ID',
						'id'		: 'ID',
						'nanotime'	: 'Nanotime'
					};
					
					var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">W Detected</th></tr></table>');
					var iCount = 1;
					for(var key in keys){
						if(iCount%2 == 1){
							var classname = 'odd';
						} else {
							var classname = 'even';
						};
						
						table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + witem[key] + '</td></tr>');
						
						iCount++;
					};
					
					table.appendTo(holder);
					
					
					
				} else if (data.length > 1){
				
					jQuery.each(data, function(index, item){
						var toggleButton = jQuery('<a class="data_table_accordion_header accordion_toggle" rel="w_detected_' + index + '">Data Object</a>');
						if(index == 0){
							toggleButton.addClass('toggle_active');
						} else {
							toggleButton.addClass('toggle_inactive');
						};
												
						var witem = item.w_detected;
						var keys = {
							'Ch' 		: 'Ch',
							'Hn'		: 'Hn',
							'PTxPwr'	: 'PtxPwr',
							'RSCP'		: 'RSCP',
							'Rn'		: 'Rn',
							'SC'		: 'SC',
							'ServL'		: 'ServL',
							'ServQ'		: 'ServQ',
							'ULTxPwr'	: 'ULTXPwr',
							'device_id'	: 'Device ID',
							'epochtime'	: 'Timestamp',
							'gps_id'	: 'GPS ID',
							'id'		: 'ID',
							'nanotime'	: 'Nanotime'					
						};
						
						var table = jQuery('<table class="data_table" cellpadding="5" cellspacing="0" width="100%"><tr class="tableheader"><th colspan="2">W Detected</th></tr></table>');
						var iCount = 1;
						for(var key in keys){
							if(iCount%2 == 1){
								var classname = 'odd';
							} else {
								var classname = 'even';
							};
							
							table.append('<tr class="' + classname + '"><td class="label">' + keys[key] + '</td><td>' + witem[key] + '</td></tr>');
							
							iCount++;
						};
						
						var container = jQuery('<div class="accordion_container" rel="w_detected_' + index + '"></div>');
						
						if(index == 0){
							container.addClass('container_active');
						} else {
							container.addClass('container_inactive').hide();
						};
						
						toggleButton.appendTo(holder);
						container.append(table).appendTo(holder);
					});
					
					bindAccordion();
					//holder.find('accordion_toggle.toggle_active').click();
				};
			};
			
			
		});
		
	};
};


function addLoaderMessage(messageText){
	var holder = jQuery('body #loader_message_holder');
	if(!holder.length){
		holder = jQuery('<div id="loader_message_holder"></div>');
		holder.appendTo(jQuery('body'));
	};
	
	var i = 1;
	var findMessage = holder.find('#loader_message_' + i);
	while(findMessage.length){
		i++;
		findMessage = holder.find('#loader_message_' + i);
	};
	
	var newMessage = jQuery('<div class="loader_message bgcolor2 border1" id="loader_message_' + i + '"></div>');
	newMessage.html(messageText).hide().appendTo(holder).slideDown(500);
	
	
	return i;
	
};

function removeLoaderMessage(messageId){
	if(messageId <= 0) return false;	
	var message = jQuery('#loader_message_holder #loader_message_' + messageId);
	if(!message.length) return false;
	message.slideUp(500,function(){
		message.remove();
	});
	var messages = jQuery('#loader_message_holder .loader_message');
	if(!messages.length) jQuery('#loader_message_holder').remove();
};



